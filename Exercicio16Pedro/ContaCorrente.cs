﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercicio16Pedro
{
    public class ContaCorrente
    {
        public double Saldo { get; set; }
        public string Numero { get; set; }
        public string NomeCliente { get; set; }
        public double Limite { get; set; }
        public bool Situacao { get; set; }

        public ContaCorrente(double saldo, string numero, string nomeCliente, double limite, bool situacao)
        {
            this.Saldo = saldo;
            this.Numero = numero;
            this.NomeCliente = nomeCliente;
            this.Limite = limite;
            this.Situacao = situacao;
        }

        public string Sacar(double valor)
        {
            if (!Situacao)
                return "Sua conta não está ativa!";
            if (Saldo <= valor)
                return "Você não possui saldo suficiente!";
            if (Limite <= valor)
                return "Você não possui limite suficiente";

            return $"Seu novo saldo é: {RetirarDinheiro(valor)} || Seu limite 15/03: {RetirarLimite(valor)}";
        }

        public double RetirarDinheiro(double valor) {
            Saldo = Saldo - valor;
            return Saldo;
        }

        public double RetirarLimite(double valor) {
            Limite = Limite - valor;
            return Limite;
        }

        public void Transferir(double valor, ContaCorrente contaCorrente)
        {
            RetirarDinheiro(valor);
            RetirarLimite(valor);
            InserirDinheiro(valor, contaCorrente);
        }

        public string Depositar(double valor)
        {
            InserirDinheiro(valor, this);
            return $"Depósito realizado com sucesso! || Novo saldo: {Saldo}";
        }

        public void InserirDinheiro(double valor, ContaCorrente contaCorrente)
        {
            contaCorrente.Saldo = contaCorrente.Saldo + valor;
        }

        public double ConsultarSaldo()
        {
            //lógica aqui
            return 0.0;
        }
    }



}
