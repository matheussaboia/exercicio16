﻿using System;

namespace Exercicio16Pedro
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ContaCorrente contaCorrente1 = new ContaCorrente(500, "0000002", "Matheus", 500, true);
            ContaCorrente contaCorrente2 = new ContaCorrente(1000, "0000001", "Pedro", 200, true);
            Console.WriteLine(contaCorrente1.Sacar(100));
            Console.WriteLine(contaCorrente1.Sacar(500));
            Console.WriteLine(contaCorrente1.Depositar(300));
            contaCorrente1.Transferir(200, contaCorrente2);

            contaCorrente2.Depositar(200);
            contaCorrente2.Sacar(50);
            contaCorrente2.Sacar(100);

            Console.WriteLine("Hello World!");
        }
    }
}
